import logging
import sys

logging.basicConfig(
    format="[%(asctime)s %(levelname)s] %(message)s",
    level=logging.INFO
)

__version__ = "1.0.0"

full_help = \
f"""
{sys.argv[0]} version 1.0.0
(C) 2023-2024 Le Bao Nguyen and contributors.

- Usage:

*  help [command]   : Show this help or a specific command's help
*  query [value]    : Find an atom by its electron number, configuration, or atomic mass
*  show [type] [+]  : Show the entire table (table parameter) or just a chemical element (standalone parameter)
*  exit             : Exit the program.

All commands accept no parameter.

- This program can write the entire electron configuration from the given input, perfect for most atoms.

- Data is stored in ./periodic-table-lookup.json, downloaded from:

https://raw.githubusercontent.com/Bowserinator/Periodic-Table-JSON/master/periodic-table-lookup.json

- Commands history is stored in ~/.periodicShell_history.

"""

query_help = \
"""
query command: show an atom with its infomations.

Usage:

*   No argument (run query alone). The command will ask you for the input later.
*   With one argument: either a electron configuration, or electron count/Z, or even the atom's atomic mass, name, symbol.
"""

show_help = \
"""
show command: show an atom with its infomations in a special format, or show the entire periodic table.

Usage:

*   No argument: the command will ask you for the input later.
*   With one argument: the same as the query command.
"""

reload_help = \
"""
reload command: reload the program. Useful for development.
"""