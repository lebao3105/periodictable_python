import logging
import re

# Data management

def getData() -> dict:
    """
    Get the full table from GitHub:)
    It's hurt to write down 118 1/2-character strings myself:(
    Thanks Bowserinator for the list!
    """

    import os, json
    get = \
        "https://raw.githubusercontent.com/Bowserinator/Periodic-Table-JSON/master/periodic-table-lookup.json"

    if not os.path.isfile("periodic-table-lookup.json"):
        logging.info("Getting the periodic table... hope it is not outdated.")
        logging.warn("Warning: all infomations used are not confirmed to be 100% correct!")
        try:
            import requests
        except:
            logging.warn(
                "Cannot import the requests module to download file."
                " If you can't, or don't want to get requests,"
                " download the file below and place it in the current directory."
            )
            print(get)
            input("Press a key to continue...")
        else:
            resp = requests.get(get)

            if not resp.json(): logging.fatal("Oh no I can't get the periodic table!"); exit(1)
    
            logging.info("File downloaded. Saving...")

            with open("periodic-table-lookup.json", "w") as f:
                f.write(json.dumps(resp.json(), indent=4))
    
            return resp.json()
        
    return json.loads(open("periodic-table-lookup.json", "r").read())

order: list[str]
fullstack: dict = getData()

def loadJSON():
    global fullstack, order, allsymbols

    runtime_dict = {}
    if not fullstack: fullstack = getData()
    logging.info("Attempting to load all datas...")

    order = fullstack.get("order")
    fullstack.pop("order", None)

    for key in fullstack:
        new = ChemicalElement()
        new.name = fullstack[key]["name"]
        new.atomic_mass = fullstack[key]["atomic_mass"]
        new.symbol = fullstack[key]["symbol"]
        new.period = fullstack[key]["period"] # ypos
        new.group = fullstack[key]["group"] # xpos
        new.number = fullstack[key]["number"]
        new.description = fullstack[key]["summary"]
        new.electron_cfg = [fullstack[key]["electron_configuration"],
                            fullstack[key]["electron_configuration_semantic"],
                            make_electron_cfg(new.number)]
        runtime_dict[new.symbol] = new

    fullstack = runtime_dict
    del runtime_dict

# -- END

# Atoms

def make_electron_cfg(Z: int) -> str:
    count = 0
    cls = 1
    room = "s"
    rooms = {
        1: ["s"],
        2: ["s", "p"],
        3: ["s", "p", "d"],
        4: ["s", "p", "d", "f"]
    }
    rooms[5] = rooms[6] = rooms[7] = rooms[4] # Legit.
    max = {
        "s": 2, "p": 6, "d": 10, "f": 14
    }
    total = 0
    result = ""
    
    if not 0 < Z <= len(fullstack):
        raise ValueError(f"Invalid element number used: not in range [1-{len(fullstack)}]")
    
    while total < Z:

        if count == max[room]:
            result += f"{cls}{room}{count} "
            count = 1
            
            if rooms[cls][-1] == room:
                cls += 1
                room = "s"
            else:
                if room == "p" and cls >= 3:
                    room = "s"
                    cls += 1
                elif room == "s" and cls >= 4:
                    # cls -= 1
                    room = "d"
                else:
                    room = rooms[cls][rooms[cls].index(room) + 1]
        else:
            count += 1
        
        total += 1

    result += f"{cls}{room}{count} "

    return result

def check_extended_cfg(full: list[str]):
    if re.compile("[A-Za-z]").findall(full[0]) is None:
        raise SyntaxError("Extended electron configuration syntax: The first element is invalid")
    
    else:
        extended = full[0].removeprefix("[").removesuffix("]")

        if not extended in fullstack:
            raise ValueError("Unknown element")

        result = make_electron_cfg(fullstack[extended].number)
        rooms = [x[:-1] for x in result.split(" ")] + [y[:-1] for y in full[:1]]

        if [z for z in rooms if rooms.count(z) > 1]:
            raise ValueError("Found duplicate rooms!")
        else:
            print(f"The full configuration is: {result + full[1:]}")

def print_table():
    """
    Prints the full periodic table.
    Colors coming soon ig;)
    """
    new: str = "1 "
    currperiod = 1
    currgroup = 1
    
    lan: list = []
    act: list = []
    
    for element in fullstack:
        period = fullstack[element].period
        group = fullstack[element].group
        number = fullstack[element].number
        
        # Lanthanide and Actinide elements
        if number in range(57, 72):
            lan.append(element)
            if not new.endswith("  * "):
                new += "  * "
            continue
        
        if number in range(89, 104):
            act.append(element)
            if not new.endswith("  **"):
                new += "  **"
            continue
        
        if period == currperiod:
            # How to explain this?
            # Period 1-3 has no element in group B,
            # So there's a big gap between 2 elements from IIA and IIIA/VIIIA
            if group - 1 > currgroup:
                space = group - currgroup
                new += f"{'    ' * space}  {element}" # How
                currgroup += space
            else:
                new += f"  {element}"
                currgroup += 1
                
            # Extra space for elements with one character in name
            if len(element) == 1: new += " "
                
            if group == 18:
                new += "\n"
                currperiod += 1
                new += f"{str(currperiod)} "
                currgroup = 1
    
    new += "\n\n    "
    for i in range(1, 19):
        if i < 3: new += f"{i}A  " # 1A and 2A
        elif 3 <= i <= 7: new += f"{i}B  " # then 3B to 7B
        elif 8 <= i <= 10: new += "8B  " # three 8Bs
        elif i in [11, 12]: new += f"{i-10}B  " # 1B and 2B next to the third 8B
        else: new += f"{i-10}A  "
    
    new += f"\n\n\t* Lanthanide (57-71): {' '.join(lan)}" \
           f"\n\t** Actinide (89-103): {' '.join(act)}"
    print(new)
        
class ChemicalElement:
    # Blocks in the periodic table will like this:
    #       number
    #           symbol
    #           name
    #       atomic_mass
    # Additional infomations added:
    # * period
    # * group
    # * description
    # * phase (metal, solid, gas)
    name: str
    atomic_mass: float
    symbol: str
    period: int
    group: int
    number: int
    description: str
    electron_cfg: list[str]

    def writeStandalone(this, addMore: bool = False):
        """
        Write a block like in the actual table.
        """
        print(f"{this.name} in periodic-table block format:\n")
        print(
            f"\t  {this.number}\n" \
            f"\t     {this.symbol}\n" \
            f"\t     {this.name}\n" \
            f"\t  {this.atomic_mass}\n"
        )
        print(f"Electron configuration (correct for most cases): {this.electron_cfg[2]}")
        print(f"Loaded electron configuration: {this.electron_cfg[0]}")
        print(f"Loaded electron configuration (extended): {this.electron_cfg[1]}\n")
        
        if addMore:
            print(f"\nGroup number: {this.group}\nPeriod: {this.period}")
            print(f"\nDescription:\n\t{this.description}")

# -- END

if __name__ == "__main__":
    loadJSON()
    # print("Get a random one here:")
    # for x in fullstack: print(x)
    # fullstack[input("Try one! > ").capitalize()].writeStandalone()
    print_table()