from prompt_toolkit.document import Document
from prompt_toolkit.shortcuts import clear # Keep this
from prompt_toolkit.validation import Validator, ValidationError

from . import table, show_help, query_help, full_help, reload_help

# Completer lists

table.loadJSON()
completer_list = {
    "query": None,
    "help": {},
    "exit": None,
    "show": {
        "table": None,
        "standalone": None
    },
    "reload": None,
    "clear": None
}

for key in completer_list:
    if key not in ["exit", "help"]:
        completer_list["help"][key] = None

for key in table.fullstack:
    completer_list[key] = None

# -- END

# Shell input validator

class InputValidator(Validator):

    def validate(this, document: Document):
        text = document.text

        if text and not text.isdigit() and not text.isdecimal():

            items = text.split(" ")

            if len(items) > 1 and items[1]:
                
                if completer_list[items[0]] != None and items[1] not in completer_list[items[0]]:
                    raise ValidationError(cursor_position=len(items[0]) + 1,
                                          message=f"Unknown parameter: {items[1]}")
            
            elif not items[0] in completer_list:
                raise ValidationError(cursor_position=0,
                                      message="Unknown command")
        
        else:
            for i, c in enumerate(text):
                if c.isdigit(): raise ValidationError(cursor_position=i, message="Digit found")
                elif c.isdecimal(): raise ValidationError(cursor_position=i, message="Decimal found")

# -- END

# Commands

def query(inp: list[str] | str):

    def element_shell():
        
        if not inp[0].capitalize() in table.fullstack:
            raise ValueError(f"Unknown element: {inp[0]}")
        
        obj = table.fullstack[inp[0]]
        get_and_do: str = ""
        while True:
            get_and_do = input(f"({inp[0]}) >>> ")
            if get_and_do.startswith("exit"): return
            if get_and_do.startswith("dir"): print(f"-> {dir(obj)}")
            try:
                what_to_call = getattr(obj, get_and_do.split(' ')[0])
                if not callable(what_to_call):
                    print(f"-> {what_to_call}")
                else:
                    if " " in get_and_do:
                        what_to_call(*tuple(get_and_do.split(" ")[1:]))
                    else:
                        what_to_call()
            except Exception as e:
                print(f"-> Exception: {e}")
    
    if not inp:
        inp = input("Input: ")
    
    if isinstance(inp, str):
        if not len(inp.split(" ")) > 1: inp = [inp]
        else: inp = inp.split(" ")

    if inp[1] == "--interpreter": # "Interpreter" mode for an element
        return element_shell()
    
    def check_cfg(target): return table.check_extended_cfg(target)
    
    try:
        int(inp[0]) # If the specified input is a number
    except:
        # If not, check if the input is extended
        # electron configuration or just an element name
        if inp[0] in table.fullstack:
            table.fullstack[inp[0]].writeStandalone()
        else:
            if len(inp) > 1: check_cfg(inp)
            else: check_cfg(inp[0])
    else:
        # Else write out the configuration
        # TODO: do more
        return print(table.make_electron_cfg(int(inp[0])))

def show(inp: list[str]):
    if not inp:
        inp = [input("What kind of data you want to show? [table, standalone] > ")]

    if inp[0] not in completer_list["show"]:
        return ValueError("Invalid answer: must be standalone or table")
    else:
        if inp[0] == "table": return table.print_table()
        elif not inp[1:]: inp[1:] = input("Put your answer here: "); return query(inp[1])
        else: return query(inp[1:])


def help(command: str):
    
    commands = {
        "show": show_help,
        "query": query_help,
        "exit": "No help for ya:)",
        "reload": reload_help,
        "clear": "Clears the screen",
        "": full_help
    }
    
    return print(commands[command])
# -- END