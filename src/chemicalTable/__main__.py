import os
import sys
from prompt_toolkit import PromptSession
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import NestedCompleter
from prompt_toolkit.history import FileHistory
from . import table, commands, __version__

if __name__ == "__main__":
    session = PromptSession(history=FileHistory(os.path.expanduser("~/.periodicShell_history")))
    completer = NestedCompleter.from_nested_dict(commands.completer_list)
    # TODO: rename these
    def go(inp: str):
        if inp.startswith("reload"):
            import importlib
            importlib.reload(table)
            importlib.reload(commands)
            print("Completed")
        elif inp.startswith("exit"): exit(0)
        else:
            for key in commands.completer_list:
                if inp.startswith(key): run(inp)
    
    def run(inp: str):
        if not " " in inp: return getattr(commands, inp)("")
        else: splits = inp.split(" "); return getattr(commands, splits[0])(splits[1:])
        
    if len(sys.argv) > 1:
        go(" ".join(sys.argv[1:]))
    else:
        print(f"Welcome to periodicTable v{__version__}!\n"
              "(c) 2023-2024 Le Bao Nguyen and contributors.\n"
              "Warning: this is not made for any serious usage, but for education purposes.\n"
              "Run help to see all available commands.")
        try:
            while True:
                text = session.prompt('> ',
                                      completer=completer,
                                      complete_in_thread=True,
                                      validator=commands.InputValidator(),
                                      auto_suggest=AutoSuggestFromHistory())
                go(text)
        except KeyboardInterrupt:
            exit()