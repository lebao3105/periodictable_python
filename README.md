## Periodic table in Python

Other cool project to look: https://github.com/lmmentel/mendeleev

As the title says, this is the periodic table, in Python.

Welp... Here's the list of working features:

* A shell for command input!

* Detailed messages!

* Reliable electron configuration just from a number between 1 - 118, or by a name, even a float number (atomic mass). For the most cases:)

* Resource downloaded from internet (probably taken from Wikipedia, assembled by someone else on GitHub). The file is saved locally and you can check it.

> Warning: this is only for educational purposes!

## How to run

In the future, grab a file from the Releases section, unpack it and run!

Else:

1. Have Python 3 (Python 3.9 confirmed working with a little modification)

2. Use pip to install requests - in order to get the resource (optional)

> The file I download is "https://raw.githubusercontent.com/Bowserinator/Periodic-Table-JSON/master/periodic-table-lookup.json" - place it in src/.

3. Run chemicalTable module in src/ directory

```
python3 -m chemicalTable [command-optional] [parameters-optional]
```

## Example usage

```
> show standalone Fe
Iron in periodic-table block format:

	 26
	    Fe
	    Iron
	 55.8452

Electron configuration (correct for most cases): 1s2 2s2 2p6 3s2 3p6 4s2 4d0
> show table
1   H                                                                   He
2   Li  Be                                          B   C   N   O   F   Ne
3   Na  Mg                                          Al  Si  P   S   Cl  Ar
4   K   Ca  Sc  Ti  V   Cr  Mn  Fe  Co  Ni  Cu  Zn  Ga  Ge  As  Se  Br  Kr
5   Rb  Sr  Y   Zr  Nb  Mo  Tc  Ru  Rh  Pd  Ag  Cd  In  Sn  Sb  Te  I   Xe
6   Cs  Ba  *   Hf  Ta  W   Re  Os  Ir  Pt  Au  Hg  Tl  Pb  Bi  Po  At  Rn
7   Fr  Ra  **  Rf  Db  Sg  Bh  Hs  Mt  Ds  Rg  Cn  Nh  Fl  Mc  Lv  Ts  Og
8   Uue

    1A  2A  3B  4B  5B  6B  7B  8B  8B  8B  1B  2B  3A  4A  5A  6A  7A  8A  

	* Lanthanide (57-71): La Ce Pr Nd Pm Sm Eu Gd Tb Dy Ho Er Tm Yb Lu
	** Actinide (89-103): Ac Th Pa U Np Pu Am Cm Bk Cf Es Fm Md No Lr
> query Fe --interpreter
(Fe) >>> number
-> 26
(Fe) >>> atomic_mass
-> 55.8452
(Fe) >>> exit
>
```

## Screenshots

See in [./screenshots](./screenshots).